package lab0;

public class Main {

	public static void main(String args[]) {
		//Construct three objects.
		Car car1 = new Car("Mercedes", 6, "Yellow");
		Car car2 = new Car("Cadillac", 0, "Pink");
		Car car3 = new Car("Ferrari", 5, "Red");



		//Report car details.
		car1.report();
		car2.report();
		car3.report();

		car2.decrementGear();
		car3.incrementGear();

		car2.gear = 70;
		car2.report();


	}


}
