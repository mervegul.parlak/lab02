package lab0;

public class Car {
	double odometer;
	String brand;
	String color;
	int gear;
	String countryOfOrigin;
	int year;


	public Car(String b, String c) {
		odometer = 0;
		brand = b;
		color=c;
		gear = 5;
		countryOfOrigin = "Turkey";
		year = 2020;

	}

	public Car() {

	}

	public Car(String b) {
		odometer= 0;
		brand = b;
		color = "White";
		gear = 0;
		countryOfOrigin = "Turkey";
		year = 2020;
	}
	public Car(String b, String c, String coo) {
		odometer = 0.;
		brand = b;
		color = c;
		gear = 0;
		countryOfOrigin = coo;
		year = 2020;
	}
	public Car(String b, String c, String coo, int y) {
		odometer = 0.f;
		brand = b;
		color = c;
		gear = 0;
		countryOfOrigin = coo;
		year = y;
	}
	public Car(String b, int g, String c) {
		odometer = 0l;
		brand = b;
		color = c;
		if (g < 0 || g > 5) {
			System.out.println("Error: Gear value must be in range [0,5]!");
			gear = 0;
		} else {
			gear = g;
		}
		countryOfOrigin = "Turkey";
		year = 2020;
	}

	public Car(String b, String c, int y) {
		odometer = 0l;
		brand = b;
		color = c;
		gear = 0;
		countryOfOrigin = "Turkey";
		year = y;
	}


	public void incrementGear() {
		if (gear >= 5) {
			System.out.println("Error: Gear value must be in range [0,5]!");
		}else{
			gear+=1;
		}

	}
	public void incrementGear(int i) {
		gear+=i;
	}
	public void decrementGear() {
		if (gear <= 0) {
			System.out.println("Error: Gear value must be in range [0,5]!");
		}else {
			gear-= 1;
		}
	}

	public void decrementGear(int d) {
		gear -= d;
	}


	public void drive(double numbersOfHoursTraveled, double kmTraveledPerHour) {
		odometer+=numbersOfHoursTraveled*kmTraveledPerHour;

	}

	public double getOdometer() {
		return odometer;
	}

	public void setColor(String newColor) {
		color = newColor;
	}
	public String getColor() {
		return color;
	}

	public void report() {
		System.out.println("This is a car that is manufactured in "+countryOfOrigin+" in "+year+".\n"+
				"It is a "+color+" "+brand+".\n"+
				"Its odometer is at "+odometer+".\n"+
				"Its gear is at "+gear+".\n");

	}

}
